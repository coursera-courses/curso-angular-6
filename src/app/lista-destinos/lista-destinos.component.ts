import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DestinoViaje} from '../models/destino-viaje';
import {DestinosApiClient} from '../models/destinos-api-client';
import {Store} from '@ngrx/store';
import {AppState} from '../app.module';
import {ElegidoFavoritoAction, NuevoDestinoAction} from '../models/destinos-viajes-state';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
  updates: string[];
  @Output() ItemAdded: EventEmitter<DestinoViaje>;
  all;
  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.ItemAdded = new EventEmitter<DestinoViaje>();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        if (data != null) {
          this.updates.push('Se ha elegido a ' + data.nombre);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje): void {
    this.destinosApiClient.add(d);
    this.ItemAdded.emit(d);
  }
  elegido(d: DestinoViaje): void {
    this.destinosApiClient.elegir(d);
  }

}
