import {DestinoViaje} from './destino-viaje';
import {BehaviorSubject, Subject} from 'rxjs';
import {AppState} from '../app.module';
import {Store} from '@ngrx/store';
import {ElegidoFavoritoAction, NuevoDestinoAction} from './destinos-viajes-state';
import {Injectable} from '@angular/core';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  constructor(private store: Store<AppState>) {
    this.destinos = [];
  }
  add(d: DestinoViaje): void{
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

  getById(id: string): DestinoViaje {
    return this.destinos.filter((d: DestinoViaje) =>  {
      return d.id.toString() === id;
    })[0];
  }

  elegir(d: DestinoViaje): void {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
