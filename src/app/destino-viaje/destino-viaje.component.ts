import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {DestinoViaje} from '../models/destino-viaje';
import {AppState} from '../app.module';
import {Store} from '@ngrx/store';
import {VoteDownAction, VoteUpAction} from '../models/destinos-viajes-state';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  // tslint:disable-next-line:no-input-rename
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4 mb-2';
  @Output() clicked: EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter<DestinoViaje>();
  }

  ngOnInit(): void {
  }
  ir(): any {
    console.log('hgfhgfh');
    this.clicked.emit(this.destino);
    return false;
  }

  voteUp(): boolean{
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(): boolean{
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
