import { Component, OnInit, } from '@angular/core';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as Mapboxgl from 'mapbox-gl';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient]
})
export class DestinoDetalleComponent implements OnInit {

  // destino: DestinoViaje;

  constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }

  ngOnInit() {
    //const  id = this.route.snapshot.paramMap.get('id');
    //this.destino = null // this.destinoApiClient.getById(id);
    (Mapboxgl as any).accessToken = environment.mapbokKey;
    var map = new Mapboxgl.Map({
    container: 'mapa-mapbox', 
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [-75.6073952, 6.2551835], // starting position LNG LTD
    zoom: 16 // starting zoom
    });
    map.addControl(new Mapboxgl.NavigationControl());
  }

}
