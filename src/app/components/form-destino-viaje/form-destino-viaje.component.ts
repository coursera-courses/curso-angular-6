import { Component, EventEmitter, forwardRef, OnInit, Output, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map,filter,debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { DestinoDetalleComponent } from '../destino-detalle/destino-detalle.component';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  minLongitud: number = 3;

  fg: FormGroup;

  searchResults: string[] = new Array();

  constructor(@Inject(forwardRef(()=> APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();

    this.fg = new FormGroup({
      nombre: new FormControl('', [Validators.required, this.nombreValidatorParametrizable(this.minLongitud)]),
      url: new FormControl('')
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log("cambio en el formulario: ", form);
    });

  }

  ngOnInit(): void {

    const elemNombre = <HTMLInputElement>document.getElementById('nombre');

    fromEvent(elemNombre, 'input')
    .pipe(
        map((e)=>(e.target as HTMLInputElement).value),
        filter(text => text.length > 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse =>{
        this.searchResults = ajaxResponse.response
      })

  }

  get f() { return this.fg.controls; }

  guardar(): boolean {

    if (this.fg.valid) {
      const d = new DestinoViaje(this.fg.value.nombre, this.fg.value.url);
      this.onItemAdded.emit(d);
      return false;
    }

    return false;

  }

  nombreValidator(control: FormControl): { [s: string]: boolean } | null {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidNombre: true };
    }
    return null;

  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: AbstractControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLogNombre: true };
      }
      return null;

    }
  }

}
